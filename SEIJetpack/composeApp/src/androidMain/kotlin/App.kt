import androidx.compose.runtime.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.ui.tooling.preview.Preview

@OptIn(ExperimentalResourceApi::class)
@Composable
@Preview
fun App() {


    val navController = rememberNavController()

    NavHost(navController, startDestination = "home") {
        composable("home") {
            Pages().HomePage(
                onPage1Click = { navController.navigate("page1") },
                onPage2Click = { navController.navigate("page2") }
            )
        }
        composable("page1") { Pages().BuyPage() }
        composable("page2") { Pages().EditPage() }
    }







}