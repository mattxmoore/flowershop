import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Flower(
    @SerialName("ID") val id: Int?,
    @SerialName("NAME") val name: String,
    @SerialName("AMOUNT") var amount: Int
)
