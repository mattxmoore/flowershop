import Requests.APIRequests
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
class Pages {


    @Composable
    fun HomePage(onPage1Click: () -> Unit, onPage2Click: () -> Unit) {

        MaterialTheme {
            Column(Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
                Button(
                    onClick = onPage1Click,
                    Modifier.fillMaxSize().weight(1f).padding(3.dp)
                ) {
                    Text("Buy Flowers")
                }
                Button(
                    onClick = onPage2Click,
                    Modifier.fillMaxSize().weight(1f).padding(3.dp)
                ) {
                    Text("Edit Inventory")
                }
            }
        }
    }

    @Composable
    fun BuyPage() {

        var flowers by remember { mutableStateOf<List<Flower>>(emptyList()) }
        val coroutineScope = rememberCoroutineScope()

        LaunchedEffect(Unit) {
            try {
                flowers = APIRequests().getRequest()
            } catch (e: Error) {
                e.printStackTrace()
            }
        }


        Column(modifier = Modifier.padding(16.dp).verticalScroll(rememberScrollState())) {

            for (flower in flowers) {
                Card(modifier = Modifier.fillMaxWidth(), elevation = 4.dp) {
                    Column(modifier = Modifier.padding(16.dp)) {
                        val amountState = remember { mutableStateOf(flower.amount) }
                        Text(text = amountState.value.toString() + " " + flower.name)
                        Button(
                            onClick = { amountState.value -= 1
                                flower.amount-=1
                                coroutineScope.launch { APIRequests().buyFlower(flower) }},
                            Modifier.align(Alignment.End)
                        ) {
                            Text("Buy 1")
                        }
                        Button(onClick = { amountState.value = 0
                            flower.amount=0
                            coroutineScope.launch { APIRequests().buyFlower(flower) }}, Modifier.align(Alignment.End)) {
                            Text("Buy All")
                        }
                    }
                }
                Spacer(modifier = Modifier.height(8.dp))
            }
        }
    }

    @Composable
    fun EditPage() {
        var flowers by remember { mutableStateOf<List<Flower>>(emptyList()) }
        val coroutineScope = rememberCoroutineScope()
        var showCreate by remember { mutableStateOf(false) }
        val integerPattern = Regex("""^\d*$""")
        var nameState by remember { mutableStateOf("") }
        var amountState by remember { mutableIntStateOf(0) }

        LaunchedEffect(Unit) {
            try {
                flowers = APIRequests().getRequest()
            } catch (e: Error) {
                e.printStackTrace()
            }
        }

        Column(modifier = Modifier.padding(16.dp).verticalScroll(rememberScrollState())) {
            for (flower in flowers) {
                Card(modifier = Modifier.fillMaxWidth(), elevation = 4.dp) {
                    Column(modifier = Modifier.padding(16.dp)) {
                        Text(text = flower.amount.toString() + " " + flower.name)
                        Button(
                            onClick = {
                                coroutineScope.launch { APIRequests().deleteFlower(flower) }
                                flowers = flowers.filter { it != flower }
                            },
                            Modifier.align(Alignment.End)
                        ) {
                            Text("Delete")
                        }
                        var showChangeAmount by remember { mutableStateOf(false) }
                        Button(onClick = {showChangeAmount = true}, Modifier.align(Alignment.End)) {
                            Text("Change Amount")
                            if(showChangeAmount){
                                AlertDialog(onDismissRequest = {showChangeAmount=false},
                                    title={Text("Insert New Amount")},
                                    confirmButton = {
                                        Button(onClick = {
                                            showChangeAmount=false
                                            flower.amount=amountState
                                            coroutineScope.launch {APIRequests().buyFlower(flower)}
                                            }){
                                            Text("Confirm")}
                                    },
                                    dismissButton = {
                                        Button(onClick = {showChangeAmount=false}){
                                            Text("Cancel")
                                        }
                                    },
                                    text={
                                        TextField(
                                            value = amountState.toString(),
                                            onValueChange = { newValue ->
                                                if (newValue.matches(integerPattern) || newValue.isEmpty()) {
                                                    amountState = if (newValue.isNotEmpty()) newValue.toInt() else 0
                                                }
                                            },
                                            modifier = Modifier.fillMaxWidth(),
                                            label = { Text("Amount") }
                                        )
                                    })
                            }
                        }

                    }
                }
                Spacer(modifier = Modifier.height(8.dp))
            }
            Button(onClick = {showCreate=true}){
                Text("Create New Flower")
            }


            if(showCreate){
                AlertDialog(onDismissRequest = {showCreate=false},
                    confirmButton = {
                        Button(onClick = {flowers+=Flower(null, nameState, amountState)
                            showCreate=false
                            coroutineScope.launch { APIRequests().newFlower(Flower(null, nameState, amountState)) }}){
                            Text("Add")
                        }
                    },
                    dismissButton = {
                        Button(onClick = {showCreate=false}){
                            Text("Cancel")
                        }
                    },
                    title={
                        Text("Insert Flower Values")
                    },
                    text = {
                        Column {
                            TextField(
                                value = nameState,
                                onValueChange = { nameState = it },
                                modifier = Modifier.fillMaxWidth(),
                                label = { Text("Name") }
                            )
                            TextField(
                                value = amountState.toString(),
                                onValueChange = { newValue ->
                                    if (newValue.matches(integerPattern) || newValue.isEmpty()) {
                                        amountState = newValue.toInt()
                                    }
                                },
                                modifier = Modifier.fillMaxWidth(),
                                label = { Text("Amount") }
                            )
                        }
                    })

            }
        }
    }
}

