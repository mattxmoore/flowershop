package Requests

import Flower
import android.nfc.Tag
import io.ktor.client.HttpClient
import io.ktor.client.plugins.kotlinx.serializer.KotlinxSerializer
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement

class APIRequests {
    private val client = HttpClient()
    suspend fun getRequest(): List<Flower>{

        val response : HttpResponse = client.request("http://10.0.2.2:8080/flowers/GetFlowers") {
                method = HttpMethod.Get
            }
        if (response.status == HttpStatusCode.OK) {
            val responseBody = response.bodyAsText()
            return Json.decodeFromString(responseBody)
        } else {
            // Handle other HTTP status codes (e.g., 404, 500, etc.) as needed
            throw Exception("Failed to fetch flowers: ${response.status}")
        }
    }

    suspend fun newFlower(newFlower : Flower){
        try{
        val response : HttpResponse = client.post("http://10.0.2.2:8080/flowers/CreateFlower"){
            contentType(ContentType.Application.Json)
            setBody(Json.encodeToString(newFlower))
        }
        } catch (e: Exception) {
            e.printStackTrace()
            // Handle exception
        } finally {
            client.close()
        }
    }

    suspend fun deleteFlower(newFlower: Flower){
        try{
            val response : HttpResponse = client.post("http://10.0.2.2:8080/flowers/DeleteFlower"){
                contentType(ContentType.Application.Json)
                setBody(Json.encodeToString(newFlower))
            }
        } catch (e: Exception) {
            e.printStackTrace()
            // Handle exception
        } finally {
            client.close()
        }
    }

    suspend fun buyFlower(newFlower : Flower){
        try{
            val response : HttpResponse = client.post("http://10.0.2.2:8080/flowers/BuyFlower"){
                contentType(ContentType.Application.Json)
                setBody(Json.encodeToString(newFlower))
            }
        } catch (e: Exception) {
            e.printStackTrace()
            // Handle exception
        } finally {
            client.close()
        }
    }


}