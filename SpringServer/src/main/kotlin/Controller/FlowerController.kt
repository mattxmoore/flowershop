package Controller

import Database.Flower
import Database.FlowerData
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.jdbc.core.JdbcTemplate

@RestController
@RequestMapping("/flowers")
class FlowerController(private val flowerData: FlowerData) {

    @GetMapping("/GetFlowers")
    fun getAllFlowers(): List<Map<String, Any>> {
        return flowerData.findAll()
    }

    @PostMapping("/CreateFlower")
    fun createFlower(@RequestBody flowerRequest: Flower) {
        flowerData.insertFlower(flowerRequest.name, flowerRequest.amount)
    }
    @PostMapping("/DeleteFlower")
    fun deleteFlower(@RequestBody flowerRequest: Flower){
        flowerData.deleteFlower(flowerRequest.name)
    }

    @PostMapping("/BuyFlower")
    fun buyFlower(@RequestBody flowerRequest: Flower){
        flowerData.buyAmount(flowerRequest.amount, flowerRequest.name)
    }


}