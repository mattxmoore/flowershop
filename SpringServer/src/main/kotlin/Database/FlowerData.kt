package Database

import jakarta.annotation.PostConstruct
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class FlowerData(private val jdbcTemplate: JdbcTemplate) {

    fun findAll(): List<Map<String, Any>> {
        return jdbcTemplate.queryForList("SELECT * FROM flowers")
    }

    fun insertFlower(name: String, amount: Int) {
        jdbcTemplate.update("INSERT INTO flowers (name, amount) VALUES (?, ?)", name, amount)
    }

    fun deleteFlower(name: String){
        jdbcTemplate.update("DELETE FROM flowers WHERE name = ?", name)
    }

    fun buyAmount(newAmount: Int, name: String){
        jdbcTemplate.update("UPDATE flowers SET amount = ? WHERE name = ?", newAmount, name)
    }

    @PostConstruct
    fun initializeDatabase() {
        jdbcTemplate.execute("""
            CREATE TABLE IF NOT EXISTS flowers (
                id SERIAL PRIMARY KEY,
                name VARCHAR(100) NOT NULL,
                amount INTEGER NOT NULL
            )
        """)

        jdbcTemplate.execute("""
            INSERT INTO flowers (name, amount) VALUES 
            ('Roses', 5),
            ('Lilies', 2),
            ('Sunflowers', 10),
            ('Tulips', 7)
        """)
    }
}